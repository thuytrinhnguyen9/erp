package net.erp.erpcore.mappers;

import net.erp.erpcore.entities.Customer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    Customer toEntity(net.erp.erpcore.models.Customer model);

    List<Customer> toEntities(List<net.erp.erpcore.models.Customer> models);

    net.erp.erpcore.models.Customer toModel(Customer entity);

    List<net.erp.erpcore.models.Customer> toModels(List<Customer> entities);
}
