package net.erp.erpcore.mappers;

import net.erp.erpcore.entities.User;
import net.erp.erpcore.models.UserCreate;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User toEntity(net.erp.erpcore.models.User model);

    User toEntity(UserCreate model);

    net.erp.erpcore.models.User toModel(User entity);


}
