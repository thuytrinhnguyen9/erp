package net.erp.erpcore.mappers;

import net.erp.erpcore.entities.Product;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    Product toEntity(net.erp.erpcore.models.Product model);

    List<Product> toEntities(Iterable<net.erp.erpcore.models.Product> models);

    List<net.erp.erpcore.models.Product> toModels(Iterable<Product> entities);

    net.erp.erpcore.models.Product toModel(Product entity);
}
