package net.erp.erpcore.mappers;

import net.erp.erpcore.entities.Project;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProjectMapper {
    Project toEntity(net.erp.erpcore.models.Project model);

    List<Project> toEntities(List<net.erp.erpcore.models.Project> models);

    net.erp.erpcore.models.Project toModel(Project entity);

    List<net.erp.erpcore.models.Project> toModels(List<Project> entities);
}
