package net.erp.erpcore.services.impl;

import lombok.var;
import net.erp.erpcore.exceptions.InvalidCredentialsException;
import net.erp.erpcore.mappers.UserMapper;
import net.erp.erpcore.models.Credentials;
import net.erp.erpcore.models.Jwt;
import net.erp.erpcore.models.User;
import net.erp.erpcore.models.UserCreate;
import net.erp.erpcore.repositories.UserRepository;
import net.erp.erpcore.services.UserService;
import net.erp.erpcore.utils.JwtUtil;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    private UserRepository userRepository;
    private JwtUtil jwtUtil;
    private UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository,
                           JwtUtil jwtUtil,
                           UserMapper userMapper) {
        this.userRepository = userRepository;
        this.jwtUtil = jwtUtil;
        this.userMapper = userMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findById(username)
                .orElseThrow(() -> new UsernameNotFoundException(username + " not found."));
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                true,
                true,
                true,
                true,
                Collections.emptyList());
    }

    @Override
    public Jwt login(Credentials credentials) throws InvalidCredentialsException {
        var existing = userRepository
                .findById(credentials.getUsername())
                .orElseThrow(InvalidCredentialsException::new);

        if (!existing.getPassword().equals(credentials.getPassword())) {
            throw new InvalidCredentialsException();
        }

        var jwt = new Jwt();
        jwt.setToken(jwtUtil.generateToken(userMapper.toModel(existing)));
        return jwt;
    }

    @Override
    public User findById(String username) throws UsernameNotFoundException {
        var entity = userRepository.findById(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return userMapper.toModel(entity);
    }

    @Override
    public User create(UserCreate userCreate) {
        var entity = userMapper.toEntity(userCreate);
        entity = userRepository.save(entity);
        return userMapper.toModel(entity);
    }
}
