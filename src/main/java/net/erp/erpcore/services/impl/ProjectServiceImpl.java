package net.erp.erpcore.services.impl;

import lombok.var;
import net.erp.erpcore.mappers.ProjectMapper;
import net.erp.erpcore.models.Project;
import net.erp.erpcore.repositories.ProjectRepository;
import net.erp.erpcore.services.ProjectService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository repository;
    private ProjectMapper projectMapper;

    public ProjectServiceImpl(ProjectRepository repository, ProjectMapper projectMapper) {
        this.repository = repository;
        this.projectMapper = projectMapper;
    }

    public List<Project> findAll() {
        var entities = repository.findAll();
        return projectMapper.toModels(entities);
    }

    public Project save(Project project) {
        var entity = projectMapper.toEntity(project);
        entity = repository.save(entity);
        return projectMapper.toModel(entity) ;
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public void deleteMultiple(List<Long> idList) {
        repository.deleteAll(repository.findAllById(idList));
    }
}


