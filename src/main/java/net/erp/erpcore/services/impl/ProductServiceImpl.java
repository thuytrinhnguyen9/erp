package net.erp.erpcore.services.impl;

import lombok.var;
import net.erp.erpcore.mappers.ProductMapper;
import net.erp.erpcore.models.Product;
import net.erp.erpcore.repositories.ProductRepository;
import net.erp.erpcore.services.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private ProductRepository repository;
    private ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository repository, ProductMapper productMapper) {

        this.repository = repository;
        this.productMapper = productMapper;
    }

    public List<Product> findAll() {
        var entities = repository.findAll();
        return productMapper.toModels(entities);
    }

    public Product save(Product product) {
        var entity = productMapper.toEntity(product);
        entity = repository.save(entity);
        return productMapper.toModel(entity);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public void deleteMultiple(List<Long> idList) {

        repository.deleteAll(repository.findAllById(idList));
    }
}
