package net.erp.erpcore.services;

import net.erp.erpcore.exceptions.InvalidCredentialsException;
import net.erp.erpcore.models.Credentials;
import net.erp.erpcore.models.Jwt;
import net.erp.erpcore.models.User;
import net.erp.erpcore.models.UserCreate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService {

    Jwt login(Credentials credentials) throws InvalidCredentialsException;

    User findById(String username) throws UsernameNotFoundException;

    User create(UserCreate user);
}
