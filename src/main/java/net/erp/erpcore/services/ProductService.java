package net.erp.erpcore.services;

import net.erp.erpcore.models.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();

    Product save(Product product);

    void deleteById(Long id);

    void deleteMultiple(List<Long> idList);
}
