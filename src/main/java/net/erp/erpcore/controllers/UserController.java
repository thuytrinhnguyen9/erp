package net.erp.erpcore.controllers;

import net.erp.erpcore.models.User;
import net.erp.erpcore.models.UserCreate;
import net.erp.erpcore.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/")
    public ResponseEntity<User> create(@Valid @RequestBody UserCreate user) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.create(user));
    }

    @GetMapping("/{username}")
    public ResponseEntity<User> get(@PathVariable("username") String username) {
        try {
            return ResponseEntity.ok(userService.findById(username));
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
