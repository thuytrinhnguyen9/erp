package net.erp.erpcore.repositories;

import net.erp.erpcore.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
