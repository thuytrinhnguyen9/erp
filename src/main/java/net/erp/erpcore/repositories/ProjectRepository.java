package net.erp.erpcore.repositories;

import net.erp.erpcore.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project,Long> {
}
