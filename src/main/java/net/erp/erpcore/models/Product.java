package net.erp.erpcore.models;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class Product {

    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private Float price;

    @NotBlank
    private String description;
}
