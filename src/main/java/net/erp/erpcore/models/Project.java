package net.erp.erpcore.models;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class Project {
    private Long id;

    @NotBlank
    private String name;

    @NotNull
    @Min(1)
    private Integer duration;

    @NotBlank
    private String status;
}
