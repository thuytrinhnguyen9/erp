package net.erp.erpcore.models;

import lombok.Data;

@Data
public class Jwt {
    private String token;
}
